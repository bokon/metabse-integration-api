const express = require('express');
const app = express();
const api = require('./api');
const bodyParser = require('body-parser');
const { generateDashboardUrl, generateQuestionUrl } = require('./urlgen');
const { getOneDashboard } = require('./api');
app.use(bodyParser.json());

app.post('/generateRaportList', async function (req, res) {
  // const userInfo = await post('chatbot/www/install'); tak bylo na froncie, koniecznie przerobic
  // const uid = userInfo.installationId
  const uid = 9709; //temporarily na sztywno
  const allDashboards = await api.getAllDashboards();
  const dashboardInfo = allDashboards.find(
    (el) => parseInt(el.description) === uid
  );
  if (!dashboardInfo) {
    res.status(404).json({ error: 'Ta funckja jest wyłączona dla tego konta' });
  } else {
    const userDashboardData = await getOneDashboard(dashboardInfo.id);
    res.status(202).json(userDashboardData.ordered_cards);
  }
});

app.post('/questionUrl', function (req, res) {
  console.log(req.body.id);
  res.status(202).json(generateQuestionUrl(req.body.id));
});

app.post('/dashboardUrl', function (req, res) {
  console.log(req.body.id);
  res.status(202).json(generateDashboardUrl(req.body.id));
});

app.listen(8082, () => {
  console.log('Listenin on port 8080');
});
