const mbApi = {
  questions: '/api/card/embeddable',
  getDashboard: '/api/dashboard/',
  getDashboards: '/api/dashboard/',
  baseUrl: 'http://localhost:3000',
  newSession: '/api/session',
  addQuestionToDashboard: '/api/dashboard/2/cards',
  auth: {
    username: '',
    password: '',
  },
  secretKey: '',
  testUrl: 'https://webhook.site/',
};

module.exports = mbApi;
