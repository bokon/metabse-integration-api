const mbApi = require('./apiconfig');
const fetch = require('node-fetch');
const sessionKey = createNewSessionKey();

async function createNewSessionKey() {
  const response = await fetch(mbApi.baseUrl + mbApi.newSession, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(mbApi.auth),
  });
  const key = await response.json();
  return key.id;
}

async function addQuestionToDashboard() {
  const response = await fetch(mbApi.baseUrl + mbApi.addQuestionToDashboard, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-Metabase-Session': await sessionKey,
    },
    body: JSON.stringify({
      cardId: 2,
      parameter_mappings: [],
    }),
  });
  const output = response;
  console.log(output);
  return output;
}

async function getAllQuestions() {
  const response = await fetch(mbApi.baseUrl + mbApi.questions, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-Metabase-Session': await sessionKey,
    },
  });
  const cards = await response.json();
  return cards;
}

async function getOneDashboard(x) {
  const response = await fetch(mbApi.baseUrl + mbApi.getDashboard + x, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-Metabase-Session': await sessionKey,
    },
  });
  const dashboard = await response.json();
  return dashboard;
}

async function getAllDashboards() {
  const response = await fetch(mbApi.baseUrl + mbApi.getDashboards, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'X-Metabase-Session': await sessionKey,
    },
  });
  const cards = await response.json();
  return cards;
}

async function createNewDashboard(dbname, dbdesc) {
  const response = await fetch(mbApi.baseUrl + mbApi.newDashboard, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-Metabase-Session': await sessionKey,
    },
    body: JSON.stringify({
      name: dbname,
      description: dbdesc,
    }),
  });
  const newDB = await response;
  return newDB;
}

module.exports = { getAllQuestions, getAllDashboards, getOneDashboard };
