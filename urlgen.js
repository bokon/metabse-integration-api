var jwt = require('jsonwebtoken');

var METABASE_SITE_URL = 'http://localhost:3000';
var METABASE_SECRET_KEY =
  '3c39a5f852e3abe6d627097c460061eaf3f22444f5278be17cbd649b7fc69df1';
const generateDashboardUrl = (dashboardID, IID) => {
  var payload = {
    resource: { dashboard: parseInt(dashboardID) },
    params: {
      iid: 9527, //teraz jest na sztywno, wcześniej było przekazywane z frontu, urgent todo: znaleźć sposób, żeby to pozyskać z poziomu backendu exprs.
    },
    exp: Math.round(Date.now() / 1000) + 10 * 60, // 10 minute expiration
  };
  var token = jwt.sign(payload, METABASE_SECRET_KEY);

  var iframeUrl =
    METABASE_SITE_URL +
    '/embed/dashboard/' +
    token +
    '#bordered=true&titled=true';
  return iframeUrl;
};

const generateQuestionUrl = (questionId, IID) => {
  var payload = {
    resource: { question: parseInt(questionId) },
    params: {
      IID: 9527, //teraz jest na sztywno, wcześniej było przekazywane z frontu, urgent todo: znaleźć sposób, żeby to pozyskać z poziomu backendu exprs.
    },
    exp: Math.round(Date.now() / 1000) + 10 * 60, // 10 minute expiration
  };
  var token = jwt.sign(payload, METABASE_SECRET_KEY);
  var iframeUrl =
    METABASE_SITE_URL +
    '/embed/question/' +
    token +
    '#bordered=true&titled=true';
  return iframeUrl;
};
module.exports = { generateDashboardUrl, generateQuestionUrl };
